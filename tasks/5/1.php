<?php
require "../vendor/autoload.php";
ini_set('display_errors', 1);

/*1. Напишите функцию, возвращающую дескриптор <img /> разметки HTML-страницы. Эта
функция должна принимать URL изображения в качестве обязательного аргумента, а также
текст надписи, ширину и высоту изображения в качестве необязательных аргументов alt,
height и width соответственно.
*/

function imeg($url, $alt=null, $width=null, $height=null){
    $imeg = '<img src="' . $url . '"';
    if (isset ($alt)) {
        $imeg .= ' alt="' . $alt . '"';
    }
    elseif (isset($width)) {
        $imeg .= ' width="' . $width . '"';
    }
    elseif (isset($height)) {
        $imeg .= ' height="' . $height . '"';
    }

    $imeg .= '/>';
    return $imeg;
}
print imeg('https://cs9.pikabu.ru/post_img/2017/11/01/4/1509515723174581687.jpg', 'Это ахуенная эльфийка');