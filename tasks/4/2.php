<?php
require "../vendor/autoload.php";
ini_set('display_errors', 1);

/*1. Согласно данным Бюро переписи населения США в 2010 году, самыми крупными в Соединенных
Штатах Америки были следующие города:
• Нью-Йорк (8175133 человек)
• Лос-Анджелес, шт. Калифорния (3792621 человек)
• Чикаго, шт. Иллинойс (2695598 человек)
• Хьюстон, шт. Техас (2100263 человек)
• Филадельфия, шт. Пенсильвания (1526006 человек)
• Феникс, шт. Аризона (1445632 человек)
• Сан-Антонио, шт. Техас (1327407 человек)
• Сан-Диего, шт. Калифорния (1307402 человек)
• Даллас, шт. Техас (1197816 человек)
• Сан-Хосе, шт. Калифорния (945942 человек)
Определите один массив (или ряд массивов), хранящий местоположение и население перечисленных
выше городов. Выведите на экран таблицу со сведениями о местоположении и
населении, а также общее население всех десяти городов.

2. Видоизмените выполнение задания в предыдущем упражнении таким образом, чтобы строки
в результирующей таблице были упорядочены сначала по населению, а затем по названиям
городов.
*/

$usa = [ 'New York, NY' => 8175133,
    'Los Angeles, CA' => 3792621,
    'Chicago, IL' => 2695598,
    'Houston, TX' => 2100263,
    'Philadelphia, PA' => 1526006,
    'Phoenix, AZ' => 1445632,
    'San Antonio, TX' => 1327407,
    'San Diego, CA' => 1307402,
    'Dallas, TX' => 1197816,
    'San Jose, CA' => 945942];


asort($usa);

print '<table><h3>Сортировка масива по значению asort()</h3><th><tr><td>City</td><td>populaton</td></tr></th>';
foreach ($usa as $city => $population){
    print "<tr><td>$city</td><td>$population</td></tr>";
}
print '</table>';
ksort($usa);

print '<br><br><br><br><br><table><h3>Сортировка масива по ключу ksort()</h3><th><tr><td>City</td><td>populaton</td></tr></th>';
foreach ($usa as $city => $population){
    print "<tr><td>$city</td><td>$population</td></tr>";
}
print '</table>';

