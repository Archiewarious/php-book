<?php
require "../vendor/autoload.php";
ini_set('display_errors', 1);


/*2. Напишите на PHP программу, вычисляющую общую стоимость трапезы в ресторане, состоящей
из двух гамбургеров по 4,95 доллара каждый, одного молочно-шоколадного коктейля
за 1,95 доллара и одной порции кока-колы за 0,85 доллара. Ставка налога на добавленную
стоимость составляет 7,5%, а чаевые без вычета налогов — 16%.
*/

$Hamburger = 4.95;
$milk_cocktail = 1.95;
$cola = 0.85;
$tax = 7.5;
$tip = 16;

$cost = 2*$Hamburger + $milk_cocktail + $cola;
$total_cost = round($cost + $cost / 100 * ($tax + $tip),2);

print "    You order :<br/>
1.    Hamburger - $Hamburger; 2 pc.<br/>
2.    Milk Cocktail - $milk_cocktail; 1 pc.<br/>
3.    Cola - $cola; 1 pc.<br/>

    Order cost without taxes and tips = $cost$; <br/>
    The cost of the order, including tax - $tax% adn tip - $tip% = $total_cost$; \n";

