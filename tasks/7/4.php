<?php
/*
4. Напишите программу, отображающую, проверяющую достоверность и обрабатывающую форму
для ввода сведений о доставленной посылке. Эта форма должна содержать поля ввода адресов
отправителя и получателя, а также размеров и веса посылки. При проверке достоверности
данных из переданной на обработку формы должно быть установлено, что вес посылки не
превышает 150 фунтов (около 68 кг), а любой из ее размеров — 36 дюймов (порядка 91 см).
Можете также допустить, что в форме введены адреса США, но в таком случае проверьте
правильность ввода обозначения штата и почтового индекса. Функция обработки формы в
вашей программе должна выводить на экран сведения о посылке в виде организованного,
отформатированного отчета
*/
?>
<html>
<form method="POST" action="">
    <p>Enter Shipping Details</p>
    <p>Your name: <input type="text" name="user_name"></p>
    <p>Recipient name: <input type="text" name="recipient_name"></p>
    <p>Your address: <input type="text" name="user_address"></p>
    <p>Recipient address: <input type="text" name="recipient_address"></p>
    <p>Packege size and weight</p>
    <p>Package Whidth: <input type="text" name="whidth"></p>
    <p>Package Height: <input type="text" name="height"></p>
    <p>Package Lenght: <input type="text" name="lenght"></p>
    <p>Package weight:<input type="text" name="weight"></p>
    <p><input type="submit" value="Проверить" name="send"></p>
</html>
<?php
//htmlentities
if ($_POST['send']){
    if (validation_errors($_POST)){
        foreach (validation_errors($_POST) as $error){
            echo "$error <br>";
        }
    }else{
        echo "<h3>thank u, oll right</h3><br>";
        echo "Your name :".htmlentities($_POST["user_name"])."<br>";
        echo "Recipient name :".htmlentities($_POST["recipient_name"])."<br>";
        echo "Your address :".htmlentities($_POST["user_address"])."<br>";
        echo "Recipient address :".htmlentities($_POST["recipient_address"])."<br><br>";
        echo "Packege Width :".htmlentities($_POST["whidth"])."<br>";
        echo "Packege Height :".htmlentities($_POST["height"])."<br>";
        echo "Packege Lenght :".htmlentities($_POST["lenght"])."<br>";
        echo "Packege weight :".htmlentities($_POST["weight"])."<br>";
    }
}
function validation_errors($form){
    $errors = [];
    $max_size = '91';
    $max_weight = '68';
    if ($form['whidth'] > $max_size || 
    $form['lenght'] > $max_size || 
    $form['height'] > $max_size){
        $errors[] .= "Error Packege size > $max_size";
    }
    if ($form['weight'] > $max_weight){
        $errors[] .= "Eror Packege weight > $max_weight";
    }
    return $errors;
}
?>