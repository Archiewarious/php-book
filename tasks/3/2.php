<?php
require "../vendor/autoload.php";
ini_set('display_errors', 1);

function tem_conv($temp_f){
    $temp_c = ($temp_f - 32) * 5 / 9;
    return $temp_c;
}

$f = -50;
while ($f <= 50){
    echo "temp f = $f temp c = ".tem_conv($f).'<br>';
    $f++;
}
